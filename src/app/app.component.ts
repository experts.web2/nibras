import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Nibras';
  sidebarClose: boolean = true;

  constructor() {
    setTimeout(() => {
      this.adjustWindowSize()
    }, 1000);
  }

  adjustWindowSize() {
    let windowSize = window.innerWidth;
    if (windowSize < 768) {
      this.sidebarClose = false;
    }
  }
}
